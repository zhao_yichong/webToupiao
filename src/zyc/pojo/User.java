package zyc.pojo;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/18 22:16
 * @version: v1.0
 * @modified By:
 */
public class User {
    /**
     * @descript : 建立对应的数据库的类
     * @author :ZhaoYicong
     * @date :2020/7/18 22:21
     * @Param: null
     * @return :
     * @throws :
     * @since :
     */

    private Integer id;
    private String username;
    private String password;
    private String email;
    private String project;
    private String jdname;
    private String jdpassword;
    private Integer targetid;
    private Integer totalvotes;
    private Integer endflag;

    public User() {
    }

    public User(Integer id, String username, String password, String email, String project, String jdname, String jdpassword, Integer targetid, Integer totalvotes, Integer endflag) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.project = project;
        this.jdname = jdname;
        this.jdpassword = jdpassword;
        this.targetid = targetid;
        this.totalvotes = totalvotes;
        this.endflag = endflag;
    }

    public User(Integer id, String username, String password, String email, String project, String jdname, String jdpassword, Integer targetid, Integer totalvotes) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.project = project;
        this.jdname = jdname;
        this.jdpassword = jdpassword;
        this.targetid = targetid;
        this.totalvotes = totalvotes;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", project='" + project + '\'' +
                ", jdname='" + jdname + '\'' +
                ", jdpassword='" + jdpassword + '\'' +
                ", targetid=" + targetid +
                ", totalvotes=" + totalvotes +
                ", endflag=" + endflag +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getJdname() {
        return jdname;
    }

    public void setJdname(String jdname) {
        this.jdname = jdname;
    }

    public String getJdpassword() {
        return jdpassword;
    }

    public void setJdpassword(String jdpassword) {
        this.jdpassword = jdpassword;
    }

    public Integer getTargetid() {
        if (targetid==null){
            targetid=0;
        }
        return targetid;
    }

    public void setTargetid(Integer targetid) {
        this.targetid = targetid;
    }

    public Integer getTotalvotes() {
        if (totalvotes==null){
            totalvotes=0;
        }
        return totalvotes;
    }

    public void setTotalvotes(Integer totalvotes) {

        this.totalvotes = totalvotes;
    }

    public Integer getEndflag() {
        if (endflag==null){
            endflag=0;
        }
        return endflag;
    }

    public void setEndflag(Integer endflag) {
        this.endflag = endflag;
    }
}
