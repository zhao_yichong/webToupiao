package zyc.dao;

import zyc.pojo.User;

import java.security.PublicKey;
import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/19 7:03
 * @version: v1.0
 * @modified By:
 */
public interface UserDao {


    //删除投该项目的所有评委
    public void deletjds(int id);

    // 查找评委信息
    public List<User> queryJdlistbyid(int id);
    //更改endflag状态
    public void updateendflag(int id,int flag);
    //更新用户信息
    public void updateUser(User user);

    //获取目标的总票数
    public int gettotalvotes(int id);
    //把总票数更新进去
    public  void updatetotalvotes(int number,int id);
    //获取所有用户
    public List<User> queryUserlist();
    //通过用户名查询
    public User queryUserByName(String name);

    //通过id 来查user

    public User queryUserByid(int id);


    //通过用户名和密码查询
    public User queryUserByNameandPassword(String name, String password);

    //保存用户 （不保存项目和评委）
    public int saveUser(User user);
    //保存评委
    public int saveJd(int id,String jdname,String jdpassword);

     //保存项目
    public int saveproget(int id,String project);

    //查询评委by名字
    public User queryJdByname(String name);

    //查询评委by名字和密码和对应的id
    public User queryJdByNameandPaswordandId(String name, String password, int id);

    //通过id修改评委密码
    public int updateJd(int id, String jdpassword);

    //通过id来判断targetid是否为空

    public boolean targetidIsnull(int id);

    //通过id来修改targetid
    public void updatetargetid(int id,int targetid);


}
