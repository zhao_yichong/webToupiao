package zyc.dao.impl;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import zyc.utils.JdbcUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/18 23:02
 * @version: v1.0.
 * @modified By:
 */
public class BaseDao {
    private QueryRunner queryRunner = new QueryRunner();

    public Object queryForsingleValue(String sql,Object...args){
        /**
         * @descript :  获取单行数据
         * @author :ZhaoYicong
         * @date :2020/7/18 23:10
         * @Param: type
         * @Param: sql
         * @Param: args
         * @return :T  null为失败
         * @throws :
         * @since :
         */

        Connection connection = JdbcUtils.getconnection();
        try {
            return queryRunner.query(connection, sql, new ScalarHandler(),args);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {

            JdbcUtils.closeconnection(connection);
        }
        return null;
    }

    public <T> List<T> queryForlist(Class<T> type, String sql, Object...args){
        /**
         * @descript :  获取多行数据
         * @author :ZhaoYicong
         * @date :2020/7/18 23:10
         * @Param: type
         * @Param: sql
         * @Param: args
         * @return :T  null为失败
         * @throws :
         * @since :
         */

        Connection connection = JdbcUtils.getconnection();
        try {
            return queryRunner.query(connection, sql, new BeanListHandler<>(type), args);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {

            JdbcUtils.closeconnection(connection);
        }
        return null;
    }



    public <T> T queryForone(Class<T> type,String sql,Object...args){
        /**
         * @descript :  获取单行数据
         * @author :ZhaoYicong
         * @date :2020/7/18 23:10
         * @Param: type
	 * @Param: sql
	 * @Param: args
         * @return :T  null为失败
         * @throws :
         * @since :
         */

        Connection connection = JdbcUtils.getconnection();
        try {
            return queryRunner.query(connection, sql, new BeanHandler<>(type), args);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {

            JdbcUtils.closeconnection(connection);
        }
        return null;
    }

    public int update(String sql,Object...args){
        /**
         * @descript : 数据更新
         * @author :ZhaoYicong
         * @date :2020/7/18 23:06
         * @Param: sql
	 * @Param: args
         * @return :int  -1表示数据更新失败
         * @throws :
         * @since :
         */

        Connection connection = JdbcUtils.getconnection();

        try {
            int update = queryRunner.update(connection, sql, args);
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.closeconnection(connection);
        }
        return -1;

    }


}
