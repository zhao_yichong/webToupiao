package zyc.dao.impl;

import zyc.dao.UserDao;
import zyc.pojo.User;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/19 7:04
 * @version: v1.0
 * @modified By:
 */
public class UserDaoImpl extends BaseDao implements UserDao {

    @Override
    public void deletjds(int id) {
        String sql="update javaweb.user set user.targetid=0 where user.targetid=?";
        update(sql,id);
    }

    @Override
    public List<User> queryJdlistbyid(int id) {
        String sql="SELECT * FROM javaweb.user WHERE user.targetid=?";
        List<User> users = queryForlist(User.class, sql, id);
        return users;
    }

    @Override
    public void updateendflag(int id, int flag) {
        String sql="update javaweb.user set endflag=? where id=?";
        update(sql,flag,id);
    }

    @Override
    public void updateUser(User user) {

        String sql="UPDATE javaweb.user SET username=?,password=?,email=?,project=?,jdname=?,jdpassword=?,targetid=?,totalvotes=? WHERE id =?";
         update(sql,user.getUsername(),user.getPassword(),user.getEmail(),user.getProject(),user.getJdname(),user.getJdpassword(),user.getTargetid(),user.getTotalvotes(),user.getId());

    }

    @Override
    public int gettotalvotes(int id) {
        String sql="SELECT COUNT(*) FROM javaweb.user WHERE targetid=?";
        int  number =Integer.parseInt(String.valueOf(queryForsingleValue(sql, id)));

        return number;
    }

    @Override
    public void updatetotalvotes(int number, int id) {
        String sql="UPDATE javaweb.user SET totalvotes=? WHERE id=?";
        update(sql,number,id);
    }

    @Override
    public List<User> queryUserlist() {
        String sql="SELECT * FROM javaweb.user ORDER BY totalvotes DESC ";
        return queryForlist(User.class,sql);
    }

    @Override
    public User queryUserByName(String name) {
            String sql="select * from javaweb.user where username=?";
            return  queryForone(User.class,sql,name);
    }

    @Override
    public User queryUserByid(int id) {
        String sql="select * from javaweb.user where id=?";
        return queryForone(User.class,sql,id);
    }

    @Override
    public User queryUserByNameandPassword(String name, String password) {
        String sql="select * from javaweb.user where username=?and password=?";
        return queryForone(User.class,sql,name,password);
    }

    @Override
    public int saveUser(User user) {
        String sql="insert into javaweb.user values(?,?,?,?,?,?,?,?,?,0)";
        return update(sql,user.getId(),user.getUsername(),user.getPassword(),user.getEmail(),user.getProject(),user.getJdname(),user.getJdpassword(),user.getTargetid(),user.getTotalvotes());

    }

    @Override
    public int saveJd(int id,String jdname, String jdpassword) {
        String sql="update javaweb.user set jdname=?,jdpassword=? where id=?";
        int update = update(sql, jdname, jdpassword, id);
        return update;
    }

    @Override
    public int saveproget(int id,String project) {
        String sql="update javaweb.user set project=? where id=?";
        int update = update(sql, project, id);
        return update;

    }

    @Override
    public User queryJdByname(String name) {
       String sql="select * from javaweb.user where  jdname=?";
       return queryForone(User.class,sql,name);
    }

    @Override
    public User queryJdByNameandPaswordandId(String name, String password, int id) {
        String sql="select * from javaweb.user where jdname=? and jdpassword=? and id=?";
        return queryForone(User.class,sql,name,password,id);
    }

    @Override
    public int updateJd(int id, String jdpassword) {
        String sql="update javaweb.user set jdpassword=? where id=?";
        return update(sql,jdpassword,id);
    }

    @Override
    public boolean targetidIsnull(int id) {
        String sql="select targetid from javaweb.user where id=?";
        int number =  Integer.parseInt(String.valueOf(queryForsingleValue(sql, id))) ;
        if (number==0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void updatetargetid(int id, int targetid) {
        String sql="update javaweb.user set targetid=? where id=?";
         update(sql, targetid, id);

    }
}
