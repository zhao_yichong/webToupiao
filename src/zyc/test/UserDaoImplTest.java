package zyc.test;

import org.junit.Test;
import zyc.dao.UserDao;
import zyc.dao.impl.UserDaoImpl;
import zyc.pojo.User;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/19 7:30
 * @version:
 * @modified By:
 */
public class UserDaoImplTest {


    UserDao userDao=new UserDaoImpl();

    @Test
    public void queryUserByName() {
        User user = userDao.queryUserByName("wyy");
        System.out.println(user);

    }

    @Test
    public void queryUserByNameandPassword() {
        User user = userDao.queryUserByNameandPassword("wyy", "1234");
        System.out.println(user);
    }

    @Test
    public void saveUser() {
        int i = userDao.saveUser(new User(null, "wyy", "1234", "1234@qq.com", "hello word", "美丽", "abcd", null, 0));

    }

    @Test
    public void queryJdByname() {
        User user = userDao.queryJdByname("伟大");
        System.out.println(user);
    }

    @Test
    public void queryJdByNameandPaswordandId() {
        User user = userDao.queryJdByNameandPaswordandId("美丽", "abcd", 2);
        System.out.println(user);
    }

    @Test
    public void updateJd() {
        int i = userDao.updateJd(1, "12345");
    }

    @Test
    public void targetidIsnull() {
        boolean b = userDao.targetidIsnull(2);
        System.out.println(b);
    }

    @Test
    public void updatetargetid() {
        userDao.updatetargetid(2,1);
    }

    @Test
    public void gettotalvotes() {
        int gettotalvotes = userDao.gettotalvotes(1);
        System.out.println(gettotalvotes);
    }

    @Test
    public void updatetotalvotes() {
        userDao.updatetotalvotes(4,1);
    }

    @Test
    public void updateUser() {
        userDao.updateUser(new User(14,"赵轶聪a","555","123","okkk","a","a",0,0));
    }

    @Test
    public void queryJdlistbyid() {
        List<User> users = userDao.queryJdlistbyid(6);
        for (User user:users
             ) {
            System.out.println(user);
        }
    }

    @Test
    public void deletjds() {
        userDao.deletjds(6);
    }
}