package zyc.test;

import org.junit.Test;
import zyc.pojo.User;
import zyc.service.UserService;
import zyc.service.impl.UserServiceImpl;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/19 8:06
 * @version:
 * @modified By:
 */
public class UserServiceImplTest {
    UserService userService=new UserServiceImpl();

    @Test
    public void loginUser() {
        User user = userService.loginUser("zyc", "1234");
        if (user==null){
            System.out.println("账户或密码错误");
        }else{
            System.out.println("登录成功");
        }
        System.out.println(user);
    }

    @Test
    public void registUser() {
        int i = userService.registUser(new User(null, "赵聪", "1234", "1234@qq.com", null, null, null, null, null));
        if (i>0){
            System.out.println("注册成功");
        }
    }

    @Test
    public void loginJd() {
        User user = userService.loginJd("物语", "12345", 3);
        if (user==null){
            System.out.println("账户或密码错误");
        }else{
            System.out.println("登录成功");
        }
        System.out.println(user);

    }

    @Test
    public void existsUser() {
        boolean b = userService.existsUser("wyy2");
        if (b){
            System.out.println("用户已存在");
        }else{
            System.out.println("用户可用");
        }
    }

}