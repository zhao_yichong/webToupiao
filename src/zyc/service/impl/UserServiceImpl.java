package zyc.service.impl;

import zyc.dao.UserDao;
import zyc.dao.impl.UserDaoImpl;
import zyc.pojo.User;
import zyc.service.UserService;

import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/19 7:57
 * @version: v1.0
 * @modified By:
 */
public class UserServiceImpl implements UserService {


    UserDao userDao=new UserDaoImpl();


    @Override
    public void deletejdsbyid(int id) {
        userDao.deletjds(id);
    }

    @Override
    public List<User> getjds(int id) {
        List<User> users = userDao.queryJdlistbyid(id);
        return users;
    }

    @Override
    public void endvote(int id,int flag) {
        userDao.updateendflag(id,flag);
    }

    @Override
    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Override
    public void updatetotalvotes(int id) {
        userDao.updatetotalvotes(userDao.gettotalvotes(id),id);
    }

    @Override
    public void registJd(int id, String jdname, String jdpassword) {
        userDao.saveJd(id,jdname,jdpassword);
    }

    @Override
    public void registProjcet(String projcet, int id) {
        userDao.saveproget(id,projcet);
    }

    @Override
    public User loginUser(String username, String password) {
        User user = userDao.queryUserByNameandPassword(username, password);

        return user;
    }

    @Override
    public int registUser(User user) {
        int i = userDao.saveUser(user);

        return i;
    }

    @Override
    public User loginJd(String jdname, String jdpassword, int id) {
        User user = userDao.queryJdByNameandPaswordandId(jdname, jdpassword, id);
       return user;

    }

    @Override
    public boolean existsUser(String name) {
        User user = userDao.queryUserByName(name);
        if (user==null){
            return false;
        }else {
            return true;
        }

    }

    @Override
    public List<User> getUsers() {
        List<User> users = userDao.queryUserlist();
        return users;
    }

    @Override
    public boolean vote(int id,int targetid) {
        if (userDao.targetidIsnull(id)){
            userDao.updatetargetid(id,targetid);
            return true;
        }
        return false;
    }

    @Override
    public boolean canclevote(int id) {
        if (!userDao.targetidIsnull(id)){
            userDao.updatetargetid(id,0);
            return true;
        }
        return false;
    }

    @Override
    public boolean targeIsnullByid(int id) {
        return userDao.targetidIsnull(id);
    }

    @Override
    public User getUserByid(int id) {
        return userDao.queryUserByid(id);
    }
}
