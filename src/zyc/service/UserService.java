package zyc.service;

import zyc.pojo.User;

import java.util.Iterator;
import java.util.List;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/19 7:52
 * @version: v1.0
 * @modified By:
 */
public interface UserService {


    //删除投该项目的所有评委
    public void deletejdsbyid(int id);
    // 查找项目的投票评委
    public List<User> getjds(int id);

    //跟换投票信息投票

    public  void endvote(int id,int flag);
    //更新用户信息
    public void updateUser(User user);

    //刷新总票数 (通过id)
    public void updatetotalvotes(int id);

    //评委注册
    public void registJd(int id,String jdname,String jdpassword);

    //项目注册

    public void registProjcet(String projcet,int id);


    //用户登录
    public User loginUser(String username, String password);


    //用户注册
    public int registUser(User user);


    //评委登录
    public User loginJd(String jdname, String jdpassword, int id);

    //检查是否重复

    public boolean existsUser(String name);


    //获取所有用户
    public List<User> getUsers();


    //投票功能  id史对应项目的id
    public boolean vote(int id,int targetid);

    //取消投票功能
    public boolean canclevote(int id);

    //通过id判断是不是没有投票对象
    public boolean targeIsnullByid(int id);

    //通过id找用户
    public User getUserByid(int id);

}
