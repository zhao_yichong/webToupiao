package zyc.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/18 22:20
 * @version: v1.0
 * @modified By:
 */
public class JdbcUtils {



    //获取数据库的链接
    private static DruidDataSource source;
    static {
        try {
            InputStream is = JdbcUtils.class.getClassLoader().getResourceAsStream("jdbc.properties");
            Properties ps=new Properties();
            ps.load(is);
            source = (DruidDataSource) DruidDataSourceFactory.createDataSource(ps);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * @descript :获取数据库链接
     * @author :ZhaoYicong
     * @date :2020/7/18 23:00
     * @Param: null
     * @return :
     * @throws :
     * @since :
     */


    public static Connection getconnection()  {

        Connection connection = null;
        try {
            connection = source.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }



    /**
     * @descript :关闭数据库链接
     * @author :ZhaoYicong
     * @date :2020/7/18 22:23
     * @Param: connection   关闭的链接
     * @return :
     * @throws :
     * @since :
     */

    public static void closeconnection(Connection connection){
        if (connection!=null){

            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
