package zyc.utils;

import org.apache.commons.beanutils.BeanUtils;

import java.util.Map;

/**
 * @description:
 * @author: ZhaoYicong
 * @date: Created in 2020/7/17 21:04
 * @version: v1.0
 * @modified By:
 */
public class Webutils {
    //将获得的map值 转到对应的bean中去

    public static <T> T copyparamToBean(Map value,T bean){
        try {
            BeanUtils.populate(bean,value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }

    //string 转换成int
    public static int stringToint(String s){
        try {
            int parseInt = Integer.parseInt(s);
            return parseInt;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
