<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>投票系统注册页面</title>
<%@include file="/pages/common/head.jsp"%>

	<script type="text/javascript">


		$(function () {

				// $(".code").click(function () {
				// 	this.src="http://localhost:8080/webbook/kaptchaServlet.jpg?d="+new Date();
				// 	// alert("123")
				// })

				$("#sub_btn").click(function () {

					//匹配用户名

					// var username=$("#username").val();
					//
					// var usernamePatt=/^\w{5,12}$/;
					//
					// if (!usernamePatt.test(username)) {
					//
					// 	$("span.errorMsg").text("用户名不合法")
					// 	return false;
					// }
			//
			// 	//匹配密码
				var  password=$("#password").val();

				// var passwordPatt=/^\w{5,12}$/;
				//
				// if (!passwordPatt.test(password)){
				// 	$("span.errorMsg").text("密码不合法");
				// 	return false;
				// }

				//确认密码
				var passwordtest=$("#repwd").val();

				if (passwordtest!=password){
					$("span.errorMsg").text("密码确认失败");
					return false;
				}

				//确认邮箱
				var email=$("#email").val();
				var emailPatt=/^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/;
				if (!emailPatt.test(email)){
					$("span.errorMsg").text("邮箱格式错误");
					return false;
				}

				//验证评委名称

				// var jdname=$("#jdname").val();
				// var jdname=$.trim(jdname);
				// if (jdname==null|| jdname==""){
				// 	$("span.errorMsg").text("评委名称为空");
				// 	return false;
				// }

				//验证评委密码

				// var jdpassword=$("#jdpassword").val();
				// var jdpassword=$.trim(jdpassword);
				// if (jdpassword==null|| jdpassword==""){
				// 	$("span.errorMsg").text("评委密码为空");
				// 	return false;
				// }

				//项目文字

				// var project=$("#project").val();
				// var project=$.trim(project);
				// if (project==null|| project==""){
				// 	$("span.errorMsg").text("项目文字为空");
				// 	return false;
				// }

				// 验证码
				// var code=$("#code").val();
				// var code=$.trim(code);
				// if (code==null|| code==""){
				// 	$("span.errorMsg").text("验证码为空");
				// 	return false;
				// }


				$("span.errorMsg").text("");
			});


		});

	</script>
<style type="text/css">
	.login_form{
		height:600px;
		margin-top: 25px;
	}
	
</style>


</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
		</div>
		
			<div class="login_banner">
			
				<div id="l_content">
					<span class="login_word">欢迎注册</span>
				</div>
				
				<div id="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>用户注册</h1>
								<span class="errorMsg">${requestScope.masg}</span>
							</div>
							<div class="form">
								<form action="userServlet" method="post">
									<input type="hidden" name="action" value="regist">
									<label>用户名称：</label>
									<input class="itxt"   type="text" placeholder="请输入用户名" value="${requestScope.username}"
										   autocomplete="off" tabindex="1" name="username" id="username" />
									<br />
									<br />
									<label>用户密码：</label>
									<input class="itxt"  type="password" placeholder="请输入密码"
										   autocomplete="off" tabindex="1" name="password" id="password" />
									<br />
									<br />
									<label>确认密码：</label>
									<input class="itxt"   type="password" placeholder="确认密码"
										   autocomplete="off" tabindex="1" name="repwd" id="repwd" />
									<br />
									<br />
									<label>电子邮件：</label>
									<input class="itxt"     type="text" placeholder="请输入邮箱地址" value="${requestScope.email}"
										   autocomplete="off" tabindex="1" name="email" id="email" />
<%--									<br />--%>
<%--									<br />--%>
<%--									<label>评委名称：</label>--%>
<%--									<input class="itxt"   value="1234" type="text" placeholder="请输入评委名称"--%>
<%--										   autocomplete="off" tabindex="1" name="jdname" id="jdname" />--%>
<%--									<br />--%>
<%--									<br />--%>
<%--									<label>评委密码：</label>--%>
<%--									<input class="itxt"  value="1234" type="text" placeholder="请输入评委密码"--%>
<%--										   autocomplete="off" tabindex="1" name="jdpassword" id="jdpassword" />--%>
<%--									<br />--%>
<%--									<br />--%>
<%--									<label>项目文字：</label>--%>
<%--									<input class="itxt"  value="1234" type="text" placeholder="请输入项目文字"--%>
<%--										   autocomplete="off" tabindex="1" name="project" id="project" />--%>
<%--									<br />--%>
<%--									<br />--%>

<%--									<label>验证码：</label>--%>
<%--									<input class="itxt" type="text" style="width: 150px;" value="abcd"   name="code" id="code"/>--%>
<%--									<img src="http://localhost:8080/webbook/kaptchaServlet.jpg"  class="code" alt="" style="width: 100px; height: 28px;">--%>
<%--									<br />--%>
<%--									<br />--%>
									<input type="submit" value="注册" id="sub_btn" />
									
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		<%@include file="/pages/common/foote.jsp"%>
</body>
</html>