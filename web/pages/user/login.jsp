<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>投票系统登录页面</title>
	<%@include file="/pages/common/head.jsp"%>
</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
		</div>
		
			<div class="login_banner">
			



<%--				评委登录--%>

				<div class="content" style="float: left">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>评委登录入口</h1>
								<a href="pages/user/registuser.jsp">立即注册</a>
							</div>
							<div class="msg_cont">
								<b></b>

								<span class="errorMsg">${ empty requestScope.jmasg?"请输入评委名称和密码以及对应的id": requestScope.jmasg}</span>
							</div>
							<div class="form">
								<form action="userServlet" method="post">
									<input type="hidden"  name="action" value="loginjd">
									<label>评委名称：</label>
									<input class="itxt" type="text" placeholder="请输入评委名称" autocomplete="off" tabindex="1" name="jdname" />
									<br />
									<br />
									<label>评委密码：</label>
									<input class="itxt" type="password" placeholder="请输入评委密码" autocomplete="off" tabindex="1" name="jdpassword" />
									<br />
									<br />
									<label>项目id：</label>
									<input class="itxt" type="text" placeholder="请输入项目id" autocomplete="off" tabindex="1" name="id" />
									<br />
									<br />
									<input type="submit" value="登录" id="sub_btn2" />
								</form>
							</div>

						</div>
					</div>
				</div>




				<div class="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>用户登录</h1>
								<a href="pages/user/registuser.jsp">立即注册</a>
							</div>
							<div class="msg_cont">
								<b></b>

								<span class="errorMsg">${ empty requestScope.masg?"请输入用户名和密码": requestScope.masg}</span>
							</div>
							<div class="form">
								<form action="userServlet" method="post">
									<input type="hidden"  name="action" value="login">
									<label>用户名称：</label>
									<input class="itxt" type="text" placeholder="请输入用户名" autocomplete="off" tabindex="1" name="username" />
									<br />
									<br />
									<label>用户密码：</label>
									<input class="itxt" type="password" placeholder="请输入密码" autocomplete="off" tabindex="1" name="password" />
									<br />
									<br />
									<input type="submit" value="登录" id="sub_btn3" />
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		<%@include file="/pages/common/foote.jsp"%>
</body>
</html>