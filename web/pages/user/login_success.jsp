<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>尚硅谷会员注册页面</title>
	<%@include file="/pages/common/head.jsp"%>
	<style type="text/css">
		h1 {
			text-align: center;
			margin-top: 200px;
		}

		h1 a {
			color:red;
		}
	</style>

	<script type="text/javascript">

		$(function () {

			<c:if test="${not empty requestScope.alert}">

			alert("${requestScope.alert}")
			</c:if>

			$("a.delete").click(function () {

				if( !confirm("你确定要删除你的项目："+$(this).parent().parent().find("td:first").text()+"？")){
					return false;
				}
			});
			$("a.end").click(function () {


				if( !confirm("你确定要结束投票吗？")){
					return false;
				}
			});
			$("a.creatP").click(function () {


				if( !confirm("你确定要创建项目吗？")){
					return false;
				}
			});
			$("a.creatJ").click(function () {


				if( !confirm("你确定要创建评委吗？")){
					return false;
				}
			});
			$("a.start").click(function () {


				if( !confirm("你确定要开启投票吗？")){
					return false;
				}
			});
		});

	</script>
</head>
<body>
<div id="header">
	<img class="logo_img" alt="" src="static/img/logo.gif" >
	<%@include file="/pages/common/loginsucces_menu.jsp"%>
</div>

<div id="main">

	<table>


		<c:if test="${not empty sessionScope.user.project}">
			<tr>
				<td>项目展示</td>
				<td>项目id</td>
				<td></td>

				<td colspan="2">操作</td>
			</tr>

			<tr>
				<td>${sessionScope.user.project}</td>
				<td>${sessionScope.user.id}</td>
				<td></td>
				<td><a   href="pages/upandcreat/creat_pr.jsp">修改</a></td>
				<td><a   href="userServlet?action=selecetjd&id=${sessionScope.user.id}">查看投票评委</a></td>
				<td><a  class="delete" href="userServlet?action=deletePr&id=${sessionScope.user.id}">删除</a></td>
				<c:if test="${sessionScope.user.endflag==1}">


					<td><a  class="start" href="userServlet?action=start">开启投票</a></td>
				</c:if>
				<c:if test="${sessionScope.user.endflag==0}">


					<td><a  class="end" href="userServlet?action=end">结束投票</a></td>
				</c:if>

			</tr>

		</c:if>



		<c:if test="${empty sessionScope.user.project}">


			<tr>

				<td>你还没创建项目！</td>
				<td><a  class="creatP" href="pages/upandcreat/creat_pr.jsp">创建项目</a></td>


			</tr>

		</c:if>

		<c:if test="${empty sessionScope.user.jdname}">


			<tr>

				<td>你还没创建评委！</td>
				<td><a  class="creatJ" href="pages/upandcreat/creat_jd.jsp">创建评委</a></td>


			</tr>

		</c:if>

		<c:if test="${not empty sessionScope.user.jdname}">
			<tr>
				<td>评委名称</td>
				<td>评委密码</td>
				<td>登录id</td>


				<td colspan="2">操作</td>
			</tr>

			<tr>
				<td>${sessionScope.user.jdname}</td>
				<td>${sessionScope.user.jdpassword}</td>
				<td>${sessionScope.user.id}</td>
				<td></td>
				<td><a   href="pages/upandcreat/creat_jd.jsp">修改信息</a></td>
			</tr>
		</c:if>











		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<%--				<td><a href="pages/manager/book_edit.jsp">添加图书</a></td>--%>
		</tr>
	</table>

</div>

<%@include file="/pages/common/foote.jsp"%>
</body>
</html>