<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>修改个人信息</title>
    <%@include file="/pages/common/head.jsp"%>

    <script type="text/javascript">
        $(function () {
            <c:if test="${not empty requestScope.alert}">
            alert("${requestScope.alert}")

            </c:if>


            $("#code_img").click(function () {
                this.src="http://121.36.81.12:8080/webToupiao_war/kaptcha.jpg?d="+new Date();
            });
            $("#update").click(function () {

                if( !confirm('你确定要提交修改信息吗？')){
                    return false;
                }
            })
        });

    </script>
    <style type="text/css">
        h1 {
            text-align: center;
            margin-top: 200px;
        }

        h1 a {
            color:red;
        }

        input {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="header">
    <img class="logo_img" alt="" src="../../static/img/logo.gif" >
    <span class="wel_word">修改用户密码</span>
    <%@include file="/pages/common/manager_menu.jsp"%>
</div>

<div id="main">
    <form action="userServlet?action=sendms" method="post">


        <table>
            <tr>
                <td>用户邮箱</td>
                <td>验证码</td>
                <td colspan="2">操作</td>

            </tr>
            <tr>


                <td><input  disabled="true" name="email" type="text" value="${sessionScope.user.email}"/></td>
                <td> <input type="text" name="code" > </td>
                <td>  <img src="http://121.36.81.12:8080/webToupiao_war/kaptcha.jpg" id="code_img"></td>
                <td><input  type="submit" value="获取邮箱验证码"/></td>

            </tr>
        </table>
    </form>
    <form action="userServlet?action=updateUser" method="post">


        <table>
            <tr>
                <td>用户名称</td>
                <td>用户新密码</td>
                <td>邮箱验证码</td>

                <td colspan="2">操作</td>

            </tr>
            <tr>
                <td><input  disabled="true" name="username" type="text" value="${sessionScope.user.username}"/></td>
                <td><input name="password" type="text" value="${sessionScope.user.password}"/></td>

                <td> <input type="text" name="code2" > </td>
                <td><input id="update" type="submit" value="修改"/></td>

            </tr>
        </table>
    </form>



</div>

<%@include file="/pages/common/foote.jsp"%>
</body>
</html>