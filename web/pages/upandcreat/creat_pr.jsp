<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>项目管理页面</title>
    <%@include file="/pages/common/head.jsp"%>
    <style type="text/css">
        h1 {
            text-align: center;
            margin-top: 200px;
        }

        h1 a {
            color:red;
        }

        input {
            text-align: center;
        }
    </style>
</head>
<body>
<div id="header">
    <img class="logo_img" alt="" src="../../static/img/logo.gif" >
    <span class="wel_word">项目创建修改处</span>
    <%@include file="/pages/common/manager_menu.jsp"%>
</div>

<div id="main">
    <form action="userServlet?action=creatPr" method="post">
<%--        <input type="hidden" name="action" value="${empty requestScope.book ?"add":"update"}">--%>
        <input type="hidden"  name="username" value="${sessionScope.user.username}">
        <input type="hidden"  name="password" value="${sessionScope.user.password}">
        <input type="hidden"  name="id" value="${sessionScope.user.id}">
        <table>
            <tr>
                <td>项目</td>
                <td>项目id（固定）</td>
                <td colspan="2">操作</td>
            </tr>
            <tr>
                <td><input name="project" type="text" value="${sessionScope.user.project}"/></td>
                <td><input  disabled="true"  type="text" value="${sessionScope.user.id}"/></td>
                <td><input type="submit" value="提交"/></td>
            </tr>
        </table>
    </form>


</div>

<%@include file="/pages/common/foote.jsp"%>
</body>
</html>