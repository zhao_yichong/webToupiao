<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>投票地点</title>
	<%@include file="/pages/common/head.jsp"%>

	<script type="text/javascript">



		$(function () {

			$("a.vote").click(function () {


				if( !confirm("你确定给"+$(this).parent().parent().find("td:first").text()+"投票吗？")){
					return false;
				}
			});

			$("a.canclevote").click(function () {


				if( !confirm("你确定取消 对"+$(this).parent().parent().find("td:first").text()+"的投票吗？")){
					return false;
				}
			});
		});


	</script>

</head>
<body>

<c:if test="${empty sessionScope.user}">

	<%
		request.getRequestDispatcher("/pages/user/login.jsp").forward(request,response);
	%>

</c:if>
	
	<div id="header">
			<img class="logo_img" alt="" src="../../static/img/logo.gif" >
			<span class="wel_word">${requestScope.head}</span>
		<%@include file="/pages/common/manager_menu.jsp"%>
	</div>
	
	<div id="main">
		<table>
			<tr>
				<td>项目负责人</td>
				<td>项目展示</td>
				<td>目前票数</td>

				<td colspan="2">操作</td>
			</tr>

			<c:if test="${not empty requestScope.users}">
				<c:forEach items="${requestScope.users}" var="user">

					<c:if test="${not empty user.project}">

						<tr>
							<td>${user.username}</td>
							<td>${user.project}</td>
							<td>${user.totalvotes}</td>

							<c:if test="${user.endflag==1}">
								<td>该项目投票已结束</td>
							</c:if>
							<c:if test="${user.endflag==0}">

								<td><a  class="vote" href="userServlet?action=vote&targetid=${user.id}">投票</a></td>
							</c:if>

						</tr>
					</c:if>

				</c:forEach>
			</c:if>

			<c:if test="${ empty requestScope.users}">

					<tr>
						<td>${requestScope.user.username}</td>
						<td>${requestScope.user.project}</td>
						<td>${requestScope.user.totalvotes}</td>
						<td></td>
						<td><a href="index.jsp">去查看结果</a></td>
						<td><a  class="canclevote" href="userServlet?action=canclevote&tragetid=${requestScope.user.id}">取消投票</a></td>


					</tr>


			</c:if>





			

			
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
<%--				<td><a href="pages/manager/book_edit.jsp">添加图书</a></td>--%>
			</tr>
		</table>
	</div>

	<%@include file="/pages/common/foote.jsp"%>
</body>
</html>